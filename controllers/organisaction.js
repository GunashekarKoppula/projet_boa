const express = require('express')
const api = express.Router()
const LOG = require('../utils/logger.js')
const find = require('lodash.find')
const remove = require('lodash.remove')
const Model = require('../models/organisation.js')
const notfoundstring = 'customer not found'

// Specify the handler for each required combination of URI and HTTP verb

// HANDLE VIEW DISPLAY REQUESTS --------------------------------------------

// GET dev1
api.get('/', (req, res) => {
  //LOG.info(`Handling GET  Index ${req}`)
  res.render('organisation/index.ejs',
    {
      title:'organisation',
      layout: 'layout.ejs'  
    })
})

api.get('/findall', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  const data = req.app.locals.organisation.query
  res.send(JSON.stringify(data))
})

// GET one JSON by ID
api.get('/findone/:id', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  const id = parseInt(req.params.id)
  const data = req.app.locals.organisation.query
  const item = find(data, { org_id: id })
  if (!item) { return res.end(notfoundstring) }
  res.send(JSON.stringify(item))
})


// GET create
api.get('/create', (req, res) => {
  LOG.info(`Handling GET /create${req}`)
  const item = new Model()
  LOG.debug(JSON.stringify(item))
  res.render('organisation/create',
    {
      title: 'Create organisation',
      layout: 'layout.ejs',
      org: item
    })
})

// GET /delete/:id
api.get('/delete/:id', (req, res) => {
  LOG.info(`Handling GET /delete/:id ${req}`)
  const id = parseInt(req.params.id)
  const data = req.app.locals.organisation.query
  const item = find(data, { org_id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
  return res.render('organisation/delete.ejs',
    {
      title: 'Delete organisation',
      layout: 'layout.ejs',
      org: item
    })
})

// GET /details/:id
api.get('/details/:id', (req, res) => {
  LOG.info(`Handling GET /details/:id ${req}`)
  const id = parseInt(req.params.id)
  const data = req.app.locals.organisation.query
  const item = find(data, { org_id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
  return res.render('organisation/details.ejs',
    {
      title: 'organisation Details',
      layout: 'layout.ejs',
      org: item
    })
})

// GET one
api.get('/edit/:id', (req, res) => {
  LOG.info(`Handling GET /edit/:id ${req}`)
  const id = parseInt(req.params.id)
  const data = req.app.locals.organisation.query
  const item = find(data, { org_id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR${JSON.stringify(item)}`)
  return res.render('organisation/edit.ejs',
    {
      title: 'Edit organisation',
      layout: 'layout.ejs',
      org: item
    })
})

// HANDLE EXECUTE DATA MODIFICATION REQUESTS --------------------------------------------

// POST new
api.post('/save', (req, res) => {
  LOG.info(`Handling POST ${req}`)
  LOG.debug(JSON.stringify(req.body))
  const data = req.app.locals.organisation.query
  const item = new Model()
  LOG.info(`NEW ID ${req.body.org_id}`)
  item.org_id = parseInt(req.body.org_id)
  item.oraganisation_name = req.body.oraganisation_name
  item.organisation_sectar = req.body.organisation_sectar
  data.push(item)
  LOG.info(`SAVING NEW organisation ${JSON.stringify(item)}`)
  return res.redirect('/organisation')
})

// POST update with id
api.post('/save/:id', (req, res) => {
  LOG.info(`Handling SAVE request ${req}`)
  const id = parseInt(req.params.id)
  LOG.info(`Handling SAVING ID=${id}`)
  const data = req.app.locals.organisation.query
  const item = find(data, { org_id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`ORIGINAL VALUES ${JSON.stringify(item)}`)
  LOG.info(`UPDATED VALUES: ${JSON.stringify(req.body)}`)
  LOG.info(`UPDATED VALUES: ${JSON.stringify(req.body.name)}`)
  item.org_id = parseInt(req.body.org_id)
  item.oraganisation_name = req.body.oraganisation_name
  item.organisation_sectar = req.body.organisation_sectar
  
  LOG.info(`SAVING UPDATED organisation ${JSON.stringify(item)}`)
  return res.redirect('/organisation')
})

// DELETE id (uses HTML5 form method POST)
api.post('/delete/:id', (req, res) => {
  LOG.info(`Handling DELETE request ${req}`)
  const id = parseInt(req.params.id)
  LOG.info(`Handling REMOVING ID=${id}`)
  const data = req.app.locals.organisation.query
  const item = find(data, { org_id: id })
  if (!item) { return res.end(notfoundstring) }
  
     remove(data, { org_id: id })
    console.log(`Permanently deleted item ${JSON.stringify(item)}`)
  
  return res.redirect('/organisation')
})

module.exports = api


