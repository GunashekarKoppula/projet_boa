const express = require('express')
const api = express.Router()
const LOG = require('../utils/logger.js')
const find = require('lodash.find')
const remove = require('lodash.remove')
const Model = require('../models/account.js')
const notfoundstring = 'customer not found'
var url = require('url');
//const ejs  = require('ejs');


// Specify the handler for each required combination of URI and HTTP verb

// HANDLE VIEW DISPLAY REQUESTS --------------------------------------------

// ejs.filters.test = function(account) {

//   //empty for test
//   console.log('hello');
//   return 'account';
  
//   };

// GET dev1
api.get('/', (req, res) => {
  //LOG.info(`Handling GET  Index ${req}`)

  let querydata= url.parse(req.url, true).query

  console.log(querydata.search)

  let  data = req.app.locals.account.query
  console.log(data);

  let tempdata=[];


  if(querydata.search){

   

    data.forEach(element => {
      
     if((element.name.toLowerCase().indexOf(querydata.search.toLowerCase())>-1)
     ||(element.organization.toLowerCase().indexOf(querydata.search.toLowerCase())>-1)){

      tempdata.push(element);
     }

    });

  }
  else{

  tempdata=data
  

  }

  res.render('account/index.ejs',
    {
      title:'Account',
      layout: 'layout.ejs',
      accounter:tempdata,
      

    })
})

api.get('/findall', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  const data = req.app.locals.account.query
  res.send(JSON.stringify(data))
})

// GET one JSON by ID
api.get('/findone/:id', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  const id = parseInt(req.params.id)
  const data = req.app.locals.account.query
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  res.send(JSON.stringify(item))
})

api.post('/search', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  console.log(req.body)
  // const id = parseInt(req.params.id)
  // const data = req.app.locals.account.query
  // const item = find(data, { _id: id })
  // if (!item) { return res.end(notfoundstring) }
  // res.send(JSON.stringify(item))
})


// GET create
api.get('/create', (req, res) => {
  LOG.info(`Handling GET /create${req}`)
  const item = new Model()
  LOG.debug(JSON.stringify(item))
  res.render('account/create.ejs',
    {
      title: 'Create account',
      layout: 'layout.ejs',
      accnt: item
    })
})

// GET /delete/:id
api.get('/delete/:id', (req, res) => {
  LOG.info(`Handling GET /delete/:id ${req}`)
  const id = parseInt(req.params.id)
  const data = req.app.locals.account.query
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
  return res.render('account/delete.ejs',
    {
      title: 'Delete Account',
      layout: 'layout.ejs',
      accnt: item
    })
})

// GET /details/:id
api.get('/details/:id', (req, res) => {
  LOG.info(`Handling GET /details/:id ${req}`)
  const id = parseInt(req.params.id)
  const data = req.app.locals.account.query
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
  return res.render('account/details.ejs',
    {
      title: 'Account Details',
      layout: 'layout.ejs',
      accnt: item
    })
})

// GET one
api.get('/edit/:id', (req, res) => {
  LOG.info(`Handling GET /edit/:id ${req}`)
  const id = parseInt(req.params.id)
  const data = req.app.locals.account.query
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR${JSON.stringify(item)}`)
  return res.render('account/edit.ejs',
    {
      title: 'Edit Account',
      layout: 'layout.ejs',
      accnt: item
    })
})

// HANDLE EXECUTE DATA MODIFICATION REQUESTS --------------------------------------------

// POST new
api.post('/save', (req, res) => {
  LOG.info(`Handling POST ${req}`)
  LOG.debug(JSON.stringify(req.body))
  const data = req.app.locals.account.query
  const item = new Model()
  LOG.info(`NEW ID ${req.body._id}`)
  item._id = parseInt(req.body._id)
  item.name = req.body.name
  item.accountNumber = req.body.accountNumber
  item.organization = req.body.organization
  data.push(item)
  LOG.info(`SAVING NEW account ${JSON.stringify(item)}`)
  return res.redirect('/account')
})

// POST update with id
api.post('/save/:id', (req, res) => {
  LOG.info(`Handling SAVE request ${req}`)
  const id = parseInt(req.params.id)
  LOG.info(`Handling SAVING ID=${id}`)
  const data = req.app.locals.account.query
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`ORIGINAL VALUES ${JSON.stringify(item)}`)
  LOG.info(`UPDATED VALUES: ${JSON.stringify(req.body)}`)
  LOG.info(`UPDATED VALUES: ${JSON.stringify(req.body.name)}`)
  item.name = req.body.name
  item.accountNumber = req.body.accountNumber
  item.organization = req.body.organization
  
  LOG.info(`SAVING UPDATED customer ${JSON.stringify(item)}`)
  return res.redirect('/account')
})

// DELETE id (uses HTML5 form method POST)
api.post('/delete/:id', (req, res) => {
  LOG.info(`Handling DELETE request ${req}`)
  const id = parseInt(req.params.id)
  LOG.info(`Handling REMOVING ID=${id}`)
  const data = req.app.locals.account.query
  const item = find(data, { _id: id })
  if (!item) { return res.end(notfoundstring) }
  
     item = remove(data, { _id: id })
    console.log(`Permanently deleted item ${JSON.stringify(item)}`)
  
  return res.redirect('/account')
})

module.exports = api
