

const express = require('express')
const api = express.Router()
const LOG = require('../utils/logger.js')
const find = require('lodash.find')
const remove = require('lodash.remove')
const Model = require('../models/category.js')
const notfoundstring = 'customer not found'

// Specify the handler for each required combination of URI and HTTP verb

// HANDLE VIEW DISPLAY REQUESTS --------------------------------------------

// GET dev1
api.get('/', (req, res) => {
  //LOG.info(`Handling GET  Index ${req}`)
  res.render('category/index.ejs',
    {
      title:'category',
      layout: 'layout.ejs'  
    })
})

api.get('/findall', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  const data = req.app.locals.category.query
  res.send(JSON.stringify(data))
})

// GET one JSON by ID
api.get('/findone/:id', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  const id = parseInt(req.params.id)
  const data = req.app.locals.category.query
  const item = find(data, { cat_id: id })
  if (!item) { return res.end(notfoundstring) }
  res.send(JSON.stringify(item))
})


// GET create
api.get('/create', (req, res) => {
  LOG.info(`Handling GET /create${req}`)
  const item = new Model()
  LOG.debug(JSON.stringify(item))
  res.render('category/create',
    {
      title: 'Create category',
      layout: 'layout.ejs',
      cat: item
    })
})

// GET /delete/:id
api.get('/delete/:id', (req, res) => {
  LOG.info(`Handling GET /delete/:id ${req}`)
  const id = parseInt(req.params.id)
  const data = req.app.locals.category.query
  const item = find(data, { cat_id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
  return res.render('category/delete.ejs',
    {
      title: 'Delete category',
      layout: 'layout.ejs',
      cat: item
    })
})

// GET /details/:id
api.get('/details/:id', (req, res) => {
  LOG.info(`Handling GET /details/:id ${req}`)
  const id = parseInt(req.params.id)
  const data = req.app.locals.category.query
  const item = find(data, { cat_id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR ${JSON.stringify(item)}`)
  return res.render('category/details.ejs',
    {
      title: 'category Details',
      layout: 'layout.ejs',
      cat: item
    })
})

// GET one
api.get('/edit/:id', (req, res) => {
  LOG.info(`Handling GET /edit/:id ${req}`)
  const id = parseInt(req.params.id)
  const data = req.app.locals.category.query
  const item = find(data, { cat_id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`RETURNING VIEW FOR${JSON.stringify(item)}`)
  return res.render('category/edit.ejs',
    {
      title: 'Edit category',
      layout: 'layout.ejs',
      cat: item
    })
})

// HANDLE EXECUTE DATA MODIFICATION REQUESTS --------------------------------------------

// POST new
api.post('/save', (req, res) => {
  LOG.info(`Handling POST ${req}`)
  LOG.debug(JSON.stringify(req.body))
  const data = req.app.locals.category.query
  const item = new Model()
  LOG.info(`NEW ID ${req.body._id}`)
  item.cat_id = parseInt(req.body.cat_id)
  item.name = req.body.name
  item.catogery_type = req.body.catogery_type
  item.tax = req.body.tax
  item.is_applicable = req.body.is_applicable
  data.push(item)
  LOG.info(`SAVING NEW customer ${JSON.stringify(item)}`)
  return res.redirect('/category')
})

// POST update with id
api.post('/save/:id', (req, res) => {
  LOG.info(`Handling SAVE request ${req}`)
  const id = parseInt(req.params.id)
  LOG.info(`Handling SAVING ID=${id}`)
  const data = req.app.locals.category.query
  const item = find(data, { cat_id: id })
  if (!item) { return res.end(notfoundstring) }
  LOG.info(`ORIGINAL VALUES ${JSON.stringify(item)}`)
  LOG.info(`UPDATED VALUES: ${JSON.stringify(req.body)}`)
  LOG.info(`UPDATED VALUES: ${JSON.stringify(req.body.name)}`)
  item.cat_id = parseInt(req.body.cat_id)
  item.name = req.body.name
  item.catogery_type = req.body.catogery_type
  item.tax = req.body.tax
  item.is_applicable = req.body.is_applicable
  
  LOG.info(`SAVING UPDATED customer ${JSON.stringify(item)}`)
  return res.redirect('/category')
})

// DELETE id (uses HTML5 form method POST)
api.post('/delete/:id', (req, res) => {
  LOG.info(`Handling DELETE request ${req}`)
  const id = parseInt(req.params.id)
  LOG.info(`Handling REMOVING ID=${id}`)
  const data = req.app.locals.category.query
  const item = find(data, { cat_id: id })
  if (!item) { return res.end(notfoundstring) }
  
     remove(data, { cat_id: id })
    console.log(`Permanently deleted item ${JSON.stringify(item)}`)
  
  return res.redirect('/category')
})

module.exports = api

