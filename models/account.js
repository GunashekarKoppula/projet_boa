/** 
*  Testers model
*  Describes the characteristics of each attribute in a tester resource.
*
* @author Gunashekar Koppula <s534628@nwmissouri.edu>
*
*/

// bring in mongoose 
// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const AccountSchema = new mongoose.Schema({

  _id: {
    type: Number,
    required: true
  },
  name: {
    type: String,
    required: true,
    unique: false // change this
  },
  accountNumber: {
    type: String,
    required: true
    
  },
  accountType:{

    type:String,
    required:true,


  }
})
module.exports = mongoose.model('Account', AccountSchema);
