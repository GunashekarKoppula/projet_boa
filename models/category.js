/** 
*  Category model
*  Describes the characteristics of each attribute in a tester resource.
*
* @author Sai Samrat Adloori <S534568@nwmissouri.edu>
*
*/

// bring in mongoose 
// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const CategorySchema = new mongoose.Schema({

    cat_id: {
    type: Number,
    required: true
  },
  name: {
    type: String,
    required: true,
    unique: false // change this
  },
  catogery_type: {
    type: String,
    required: true
    
  },
  tax:{

    type:Number,
    required:true,
  },
  is_applicable:{

    type:Boolean,
    required:true,
  },
 
})
module.exports = mongoose.model('category', CategorySchema);
