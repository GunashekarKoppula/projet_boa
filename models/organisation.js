/** 
*  Testers model
*  Describes the characteristics of each attribute in a tester resource.
*
* @author Akhil Reddy Busireddy <S534779@nwmissouri.edu>
*
*/

// bring in mongoose 
// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const OrganisationSchema = new mongoose.Schema({

  org_id: {
    type: Number,
    required: true
  },
  oraganisation_name: {
    type: String,
    required: true,
    unique: true // change this
  },
  organisation_sectar: {
    type: String,
    required: true
    
  }
})
module.exports = mongoose.model('Organisation', OrganisationSchema);
