/** 
*  Testers model
*  Describes the characteristics of each attribute in a tester resource.
*
* @author Subrahmanya Sai Bharadwaj Gandrakota <S534596@nwmissouri.edu>
*
*/

// bring in mongoose 
// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const TranscationSchema = new mongoose.Schema({

  _id: {
    type: Number,
    required: true
  },
  name: {
    type: String,
    required: true,
    unique: false // change this
  },
  accountNumber: {
    type: String,
    required: true
    
  },
  transacation_number:{

    type:String,
    required:true,
  },
  transacation_type:{

    type:String,
    required:true,
  },
  transaction_date:{

    type:String,
    required:true,
  }
})
module.exports = mongoose.model('Transcation', TranscationSchema);
