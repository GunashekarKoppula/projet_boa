const Datastore = require('nedb')        // set up a temporary (in memory) database 
const accountData = require('../data/account.json')  // read in data file 
const transactionData = require('../data/transaction.json')
const organisationData = require('../data/organisation.json')
const categoryData = require('../data/category.json')


// inject Express app to configure it - EVERYTHING in through argument list

module.exports = (app) => {

  console.log('START data seeder.')
  const db = {}  // empty object to hold all collections


  db.account = new Datastore()  // new object property 
  db.account.loadDatabase()     // call the loadDatabase method

  db.transaction = new Datastore()  
  db.transaction.loadDatabase()

  db.organisation = new Datastore()  
  db.organisation.loadDatabase()


  db.category = new Datastore()  
  db.category.loadDatabase()
  //db.testers = new Datastore()  // new object property 
  //db.testers.loadDatabase()  

  // insert the sample data into our datastore
  //db.testers.insert(testerData)
  db.account.insert(accountData);

  db.transaction.insert(transactionData);

  db.organisation.insert(organisationData);

  db.category.insert(categoryData);

  // initialize app.locals (these objects are available to the controllers)
  app.locals.account = db.account.find(accountData)
  app.locals.transaction = db.transaction.find(transactionData)
  app.locals.organisation = db.organisation.find(organisationData)
  app.locals.category = db.category.find(categoryData)
  //app.locals.testers = db.developers.find(testerData)
  console.log(`${app.locals.account.query.length} accounts seeded`)

  console.log(`${app.locals.transaction.query.length} transactions seeded`)

  console.log(`${app.locals.organisation.query.length} organisations data seeded`)

  console.log(`${app.locals.category.query.length} categories data seeded`)
   
  console.log('END Data Seeder. Sample data read and verified.')
}
