# Assignment Module6 :
## Project_boa
 
#Banking App

## Overview
This app is about the account details and transactions of the users with detailed categories of the transactions made by users.

## Team:
- Gunashekar koppula
- Akhil reddy
- Bharadwaj gandrakota
- Sai samrat adloori

## Link to the Repository

- Repo <https://bitbucket.org/GunashekarKoppula/projet_boa/src/master/>


## Link to the Heroku

- Repo <https://projectbobo.herokuapp.com/>

# node-express

> Node.js and the Express web framework make it easy to build well-designed web apps using MVC

## View Web App

Open browser to the location displayed, e.g. http://localhost:3006/
